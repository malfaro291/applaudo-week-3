class OrderLine < ApplicationRecord
  belongs_to :product
  belongs_to :order

  before_save :calculate_order_line_price, :calculate_total
  before_update :restore_product_stock, :restore_order_total
  after_save :update_order_total, :update_product_stock

  validate :stock_limit

  private

  def stock_limit
    product_stock = Product.find(product_id).stock
    if quantity > product_stock
      errors.add(:quantity, "There's not enough product in stock")
    end
  end

  def restore_product_stock
    product = Product.find(product_id)
    product.stock += quantity_was
    product.save
  end

  def update_product_stock
    product = Product.find(product_id)
    product.stock -= quantity
    product.save
  end

  def calculate_total
    self.total = quantity * price
  end

  def calculate_order_line_price
    product_price = Product.find(product_id).price
    self.price = product_price
  end

  def restore_order_total
    order = Order.find(order_id)
    order.total -= self.total_was
    order.save
  end

  def update_order_total
    order = Order.find(order_id)
    order.total += total
    order.save
  end
end
