class Product < ApplicationRecord
  has_many :order_lines
  has_many :orders, through: :order_lines

  validates :sku, presence: true
  validates :name, presence: true
  validates :description, presence: true
  validates :price, presence: true
  validates :stock, presence: true

  default_scope { order(name: :asc, price: :desc) }
  scope :most_purchased_product, -> { joins(:order_lines).group("products.name").having("sum(order_lines.quantity) = ?", OrderLine.order("sum_quantity DESC").group("product_id").sum("quantity").first[1]).sum("order_lines.quantity") }
  scope :name_greater_than_two_letters, -> { where("name ~ '[^[:blank:]]{3,}'") }
end
