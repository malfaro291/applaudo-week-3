class Order < ApplicationRecord
  belongs_to :customer
  has_many :order_lines
  has_many :products, through: :order_lines

  enum check_status: {
    cancelled: 'cancelled',
    delivered: 'delivered',
    in_transit: 'in_transit',
    payment_due: 'payment_due',
    pick_up_available: 'pick_up_available',
    problem: 'problem',
    processing: 'processing',
    returned: 'returned'
  }

  validates :date, presence: true
  validates :total, presence: true
  validates :status, presence: true

  scope :join_customer, -> { joins(:customer) }
  scope :customer_between_dates, ->(first_date, second_date, customer_id) { join_customer.where('date between ? and ? and customers.id = ?', first_date, second_date, customer_id).select('orders.id, date, total, status') }
end
