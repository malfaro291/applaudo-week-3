# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Customers
5.times do
  Customer.create(first_name: Faker::Name.first_name, last_name: Faker::Name.last_name, address: Faker::Address.full_address, phone: Faker::PhoneNumber.phone_number)
end

#Products
20.times do
  product = Faker::Commerce.product_name
  Product.create(sku: '%010d' % rand(10**10), name: product, description: product, price: Faker::Commerce.price(10..100.0), stock: rand(1..25))
end

#Orders
10.times do
  Order.create(customer_id: Customer.all.sample.id, date: Faker::Date.backward(50), status: Order.check_statuses.values.sample)
end

#Order Lines
75.times do
  product = Product.all.sample
  ol = OrderLine.create(product_id: product.id, order_id: Order.all.sample.id, quantity: rand(1..25))
  ol.errors.each do |attr, msg|
    puts "#{attr}: #{msg}"
  end
end
