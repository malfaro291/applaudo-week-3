class CreateOrderLines < ActiveRecord::Migration[5.2]
  def change
    create_table :order_lines do |t|
      t.integer :product_id
      t.integer :order_id
      t.integer :quantity
      t.decimal :price
      t.decimal :total
      t.timestamps
    end
  end
end
