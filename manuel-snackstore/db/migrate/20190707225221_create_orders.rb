class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.integer :customer_id
      t.date :date
      t.decimal :total, default: 0
      t.timestamps
    end
  end
end
