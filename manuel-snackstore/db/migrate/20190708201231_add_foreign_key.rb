class AddForeignKey < ActiveRecord::Migration[5.2]
  def change
    add_foreign_key :orders, :customers, on_delete: :cascade
    add_foreign_key :order_lines, :orders, on_delete: :cascade
    add_foreign_key :order_lines, :products, on_delete: :cascade
  end
end
