class AddStatusToOrder < ActiveRecord::Migration[5.2]
  def up
    execute <<-SQL
      CREATE TYPE check_status as ENUM ('cancelled', 'delivered', 'in_transit', 'payment_due', 'pick_up_available', 'problem', 'processing', 'returned');
      ALTER TABLE orders ADD status check_status;
    SQL
  end

  def down
    remove_column :orders, :status

    execute <<-SQL
      DROP TYPE check_status
    SQL
  end
end
