# 1. Find lines where an order belongs
OrderLine.where("order_id = ?", OrderLine.all.sample.order_id)

# 2. Select all orders from an order line
Order.joins(:order_lines).where("order_lines.id = ?", OrderLine.ids.sample)

# 3. Orders that contains X product
Product.find(Product.joins(:order_lines).sample.id).orders

# 4. Total of sales of X product
Product.unscoped.joins(:order_lines).group("products.name").where("products.id = ?", OrderLine.all.sample.product_id).sum("order_lines.quantity")

# 5. Customers who bought a product with a price greater than $60, sort by product name
# (Show customer, product name and order information)
Customer.joins(orders: [{order_lines: :product}]).order("products.name").where("order_lines.price > 60").pluck('customers.first_name, customers.last_name, products.name, order_lines.price, order_lines.total, orders.date')

# 6. Select all orders between X and Y dates
Order.where(date: ((Time.now.midnight - 25.day)..Time.now.midnight))

# 7. Count the total of customer who buy a product, with the amount of product ordered desc by total customer
Product.unscoped.joins(order_lines: [{ order: :customer }]).order("count(customers.id) DESC").group("products.name").pluck("products.name, count(distinct customers.id), sum(order_lines.quantity)")

# 8. Select All the products a X Customer has bought ordered by date
Customer.joins(orders: [{order_lines: :product}]).where("customers.id = ?", Order.all.sample.customer_id).order("orders.date desc").pluck('products.name, orders.date')

# 9. Select the total amount of products a X customer bought between 2 dates
Customer.joins({orders: :order_lines}).where("orders.date BETWEEN ? AND ? AND customers.id = ?", (Time.now.midnight - 35.day), Time.now.midnight, Order.all.sample.customer_id).group("concat(customers.first_name, ' ', customers.last_name)").sum("order_lines.quantity")

# 10. Select what is the most purchased product
Product.unscoped.joins(:order_lines).group("products.name").having("sum(order_lines.quantity) = ?", OrderLine.order("sum_quantity DESC").group("product_id").sum("quantity").first[1]).sum("order_lines.quantity")

# 11. Update products stock to 10 when stock is smaller than 3
Product.where("stock < 3").update_all(stock: 10)

#SCOPES
# 12. Select all orders between `X` and `Y` for X customer
Order.customer_between_dates((Time.now.midnight - 35.day), Time.now.midnight, Order.all.sample.customer_id)

# 13. Create filter name (default) and by price
# default_scope { order(name: :asc, price: :desc) }
# 14. Select what is the most purchased product
Product.unscoped.most_purchased_product

# 15. Search product that contains words greater than two letters
Product.name_greater_than_two_letters

#VALIDATIONS
# Create one product without name
Product.create(sku: '%010d' % rand(10**10), description: Faker::Commerce.product_name, price: Faker::Commerce.price(10..100.0), stock: rand(1..25)).errors.messages

# Update product without price
Product.find(5).update(price: nil)

# Create customer without name
Customer.create(address: Faker::Address.full_address, phone: Faker::PhoneNumber.phone_number).errors.messages
